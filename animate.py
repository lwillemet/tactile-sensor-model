#! python3
import matplotlib.pyplot as plt
from matplotlib import animation            
# from matplotlib import colors as mcolors #Imported to create plots of same color, see notes.txt
import datetime
import os
import numpy as np

class AnimationClass: 
    def __init__(self,U,Fgrip,pos,t,sFspring,sFcontact,ttotal, SM, nr_nodes, save=True):
        self.N = nr_nodes               #nr of nodes
        self.U = U                      #displacements of all nodes over time
        self.Fgrip = Fgrip              #force on rigid sensor part over time
        self.sFspring = sFspring        #spring force over time
        self.sFcontact = sFcontact      #contact force over time        
        self.pos = pos                  #positions of all nodes over time
        self.framesavail = t            #total nr of timesteps
        self.ttotal = ttotal            #array with all times that have an ode solution
        self.SM = SM                    #safety margin over time
        self.save = save                #save create gif, 0/1
        self.workdir = os.getcwd()      #obtain current directory to save gif

        #Initialize animation
        self.fig = plt.figure(figsize=(10,8)) 
        self.ax = plt.axes()
        self.ax.hlines(0,-0.01,0.01, linewidth=0.5,color='k')
        self.line, = self.ax.plot([], [] , marker='o',markersize=2, color='grey', linestyle='-', linewidth=2)   #Outline sensor
        self.rigpos, = self.ax.plot([], [] , marker='o',markersize=3, color='k', linewidth=3)                   #Rigid sensor part
        self.texts = self.ax.text(-0.008,0.009,'', fontsize=15)                     #Display time and sm
        self.textgrip = self.ax.text(-0.003,0.006,'', fontsize=10, color = 'red')   #Display value grip force
        self.textcontact = self.ax.text(0,-0.002,'', fontsize=10, color='blue')     #Display value contact force
        self.ax.grid()
        plt.axis('equal')
        plt.title('Motion and external force development \n\n Grip force = red, Contact force = blue \n SM = Safety Margin') #or plt.fig.suptitle?
        plt.xlabel('y (m)')
        plt.ylabel('x (m)')

        self.quiverpos = self.pos.copy().T          #TODO: cumbersome way to initialize positions for animation? 
        self.quivgrip = self.Fgrip.copy().T         #TODO: cumbersome way to initialize positions for animation? 
        self.quivcont = self.sFcontact.copy().T     #TODO: cumbersome way to initialize positions for animation? 
        self.Fgrip_anim = self.ax.quiver(self.quiverpos[0][0],self.quiverpos[0][self.N+1],-self.quivgrip[0][0],-self.quivgrip[0][self.N+1], scale=0.1, color='red') 
        self.Fcont_anim = self.ax.quiver(self.quiverpos[0][1:self.N+1],self.quiverpos[0][self.N+2:],-self.quivcont[0][1:self.N+1],-self.quivcont[0][self.N+2:], scale=0.1, color='blue')      

        self.y_rigpart = np.vstack((self.pos[self.N+2,:],self.pos[-1,:])) # Create array that contains rigid sensor part x positions for plotting
        self.x_rigpart = np.vstack((self.pos[1,:],self.pos[self.N,:]))    # Create array that contains rigid sensor part y positions for plotting

    def animate(self,i):
        tt = 50        #influence speed of gifs, only display every 50th time
        mid = int(np.ceil(self.N/2))   #index central node

        self.line.set_ydata(self.pos[self.N+1:,tt*i])  # Position outline sensor x 
        self.line.set_xdata(self.pos[0:self.N+1,tt*i]) # Position outline sensor y

        self.rigpos.set_ydata(self.y_rigpart[:,tt*i])   # Position rigid sensor part x 
        self.rigpos.set_xdata(self.x_rigpart[:,tt*i])   # Position rigid sensor part y

        smstamp = "{:.2f}".format(self.SM[i*tt]) # Display timestamp and safety margin in animation
        if self.SM[i*tt] < 0.01: 
            smstamp = "Full slip"
        self.texts.set_text('Time = %.1f sec, SM = %s'%(self.ttotal[i*tt], smstamp))   

        textgrip = 'Fy = %.3f N \nFx = %.3f N' %(self.Fgrip[self.N+1,0], self.Fgrip[0,0]) #Display grip and contact forces
        Fxc = np.sum(self.sFcontact[2:self.N,tt*i],axis=0)
        Fyc = np.sum(self.sFcontact[self.N+3:-1,tt*i], axis=0)
        textcontact = 'Fy = %.3f N \nFx = %.4f N\nMax. Fx = \u03BC*Fn = 0.2*%.3f = -0.002 N' %(Fyc, Fxc, self.Fgrip[self.N+1,0]) #TODO: max Fx changes when Grip changes
        self.textgrip.set_text(textgrip)
        self.textcontact.set_text(textcontact)

        self.Fgrip_anim.set_offsets(np.stack((self.quiverpos[tt*i][0],self.quiverpos[tt*i][self.N+1]))) #Arrows grip force x
        self.Fgrip_anim.set_UVC(self.quivgrip[tt*i][0],self.quivgrip[tt*i][self.N+1])                   #Arrows grip force y

        self.Fcont_anim.set_offsets(np.stack((self.quiverpos[tt*i][1:self.N+1],self.quiverpos[tt*i][self.N+2:]),axis=1))  #Arrows contact force x
        self.Fcont_anim.set_UVC(self.quivcont[tt*i][1:self.N+1],self.quivcont[tt*i][self.N+2:])                           #Arrows contact force y

        return self.line, self.texts, self.textgrip, self.textcontact, self.Fgrip_anim, self.Fcont_anim, self.rigpos, 

    def run(self):
        anim = animation.FuncAnimation(self.fig, self.animate, blit=True, frames = int(np.floor(self.framesavail/50)), repeat=True)
        
        if self.save == True: #Save gif
            date_string = datetime.datetime.now().strftime("%d%m%Y%H%M%S")
            f = (self.workdir + '\\Figures\\WorkingModel-%s.gif' % date_string)
            writergif = animation.PillowWriter(fps=30)  
            anim.save(f, writer=writergif)

        plt.show()
        a = 1


