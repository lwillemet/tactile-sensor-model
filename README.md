# Tactile sensor model
There are 2 files in this code: 
- solverfile: uses the solve_ivp function to obtain a solution
- animate: creates some plots and animations for both previous files

## Project status
### solverfile.py
def init:
Defines all variables needed to determine sensor shape, spring stiffnesses and damping. Also initial force on rigid sensor part is defined here. The nodes of the finit element model are mostly located at the elastic sensor part where they are spread evenly across the surface. There are three nodes on the rigid sensor part (left, right, middle).  

def solveODE: 
Launches the function controlFunc that contains the ode solver (solve_ivp). When solver succeeds it uses the solution to calculate the forces on intermediate timesteps. 

def controlFunc: 
Cumbersome function that launches the ode solver (solve_ivp). It throws some error messages if the solver doesn't succeed. The solver is interrupted when SM<0.2. In this case the normal grip force is raised with 0.01 N and the solver restarts with new force and current position as initial values. The solver does not restart when it successfully reaches the final timestep without SM < 0.2. Lastly a very cumbersome for loop is used to append all solutions nicely so we can plot them as one solution. 

### animate.py
This file creates a gif that shows displacement and external force development over time. 

## Old files
- fingertip_python.py: solves differential equations with own RK4 solver and is my own implementation.
- vector_python.py: very similar to fingertip_python, but a variant of Michael's code to check if my code was correct. Uses RK4 too, but same rotation happens. 
Probably depends on predefined timestep, since it doesn't happen with the solve_ivp function.  

Questions: 
- What is the time constant of Dahl friction? (internet says 1/v, but no correct unit?)
