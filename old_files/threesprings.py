#! python3
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.integrate import solve_ivp
from animatesimple import AnimationClass

class Fingertip:
    def __init__(self):
        # sensor structure
        self.r = 8e-3               # radius radius in m
        self.N1 = 3               # number of nodes
        self.d0 = 1e-5              # Initial height of center in m

        self.sigma0 = 1e4;          # rest stiffness of the bristle (N/m)       
        self.n = 1 #0.7               # exponent: material dependent parameter
        self.fclb = 0.2             # coulomb friction coefficient

        self.Fs = 1e5               # samping frequency (Hz)

        # External forces applied on the bone
        self.P = -0.15               # initial pressure (N)       
        self.Q = 0.03 #0.1          # initial pressure (N)       

        # initial parameters
        self.x = np.array([0, -self.r, 0, self.r])
        self.y = np.array([self.r+self.d0, self.r+self.d0, self.d0, self.r+self.d0])   
        self.newpos = np.concatenate((self.x, self.y))         #[xb x1 x2 .. xn yb y1 y2 ... yn]

        l00 = self.r
        l01 = np.sqrt(np.power(self.r, 2) + np.power(self.r, 2))
        l02 = l01
        self.l0 = np.array([l00,  l01, l02])

        #Damping and spring stiffnesses  
        self.kc = 1e2          # Stiffness surface (N/m)
        self.kext = 100 #100         # Stiffness external springs (N/m)
        self.c = 1                  # damping ratio (-)


    def rungeKutta(self):
        #Initial force and diplacement matrices 
        Fgrip = np.zeros(2*(self.N1+1))            #grip force vector F = [Fx, Fy]
        Fgrip[self.N1+1] = self.P                  #apply downward force on node 0
        Fgrip[0] = self.Q                        #apply sideward force on node 0

        u0 =  np.zeros(3*(self.N1+1))  #Initial values solver; u0 = [ux, uy, fcontx]

        outputu = solve_ivp(self.dydx,(0,5),u0, args=[Fgrip])
        # Fgrip[0] = self.Q
        # u02 = outputu.y[:,-1]
        # outputu2 = solve_ivp(self.dydx,(0,0.5),u02, args=[Fgrip])

        U = outputu.y[0:2*(self.N1+1),:]    #ONE SOLVER displacement per timestep 
        # U = np.concatenate((outputu.y[0:2*(self.N1+1),:],outputu2.y[0:2*(self.N1+1),:]),axis=1)    #TWO SOLVERS displacement per timestep 

        pos = U + self.newpos[:,None]       #position per timestep

        # obtain forces and indices at each timestep
        # t = outputu.t.size + outputu2.t.size    # TWO SOLVERS
        t = outputu.t.size                      # ONE SOLVER
        columsnr = t
        sFcontact = np.zeros((2*(self.N1+1),columsnr))
        sFspring = np.zeros((2*(self.N1+1),columsnr))
        sIndex = np.zeros((self.N1+1,columsnr))

        for ii in range(columsnr):
            iets, sFspring[:,ii] = self.calculateFspring(U[:,ii]) 
            sFcontact[self.N1+1:,ii], ind, sIndex[:,ii] = self.calculateFcontacty(U[:,ii])

        sFcontact[0:self.N1+1] = outputu.y[2*(self.N1+1):,:] #ONE SOLVER
        # sFcontact[0:self.N1+1] = np.concatenate((outputu.y[2*(self.N1+1):,:],outputu2.y[2*(self.N1+1):,:]),axis=1) #TWO SOLVERS

        Fgrip = np.zeros((2*(self.N1+1),columsnr))
        Fgrip[0,:] =self.Q
        Fgrip[self.N1+1,:] =self.P

        # ttotal = np.concatenate((outputu.t, outputu2.t+outputu.t[-1] )) #TWO SOLVERS
        ttotal = outputu.t     #ONE SOLVER

        # Safety Margin
        SM = (self.fclb*self.P - sFcontact[2,:])/(self.fclb*self.P) #calculate safety margin

        return U, Fgrip, pos,t, sFspring, sFcontact, ttotal , SM

    def dydx(self, t, U , Fgrip): 
        Uxy = U[0:2*(self.N1+1)]
        Fxc = U[2*(self.N1+1):3*(self.N1+1)]

        # Spring forces
        Fspring, plotspring = self.calculateFspring(Uxy)  

        # Contact forces
        Fyc, ind, indexvector = self.calculateFcontacty(Uxy) #contact force in y direction and contact indices at time t

        # Total external forces
        Fext = np.concatenate((np.zeros(self.N1+1),Fyc)) + np.concatenate((Fxc, np.zeros(self.N1+1))) + Fgrip #Fcontact + Fgrip
        
        #Differential equation for dUx and dUy
        dUxy = Fspring/self.c + Fext/self.c
        
        # Friction (case of contact)
        dUx = dUxy[0:self.N1+1] 
        dUFxc = np.zeros(self.N1+1)

        if ind[0].size != 0: #Differential equation for dFxcontact
            dF1 = self.sigma0*dUx[ind[0]] 
            dF2 = np.power(np.abs(1+np.divide(Fxc[ind[0]],self.fclb*Fyc[ind[0]])*np.sign(dUx[ind[0]])),self.n) 
            dFsigns = np.sign(1+np.divide(Fxc[ind[0]],self.fclb*Fyc[ind[0]])*np.sign(dUx[ind[0]]))
            dUFxc[ind[0]] = - dF1*dF2*dFsigns
            
        dU = np.concatenate((dUxy,dUFxc))

        # Constraints
        dU[1] = dU[3] = dU[0] #x
        dU[self.N1+2] = dU[self.N1+4] = dU[self.N1+1] #y
        return dU

    def calculateFspring(self,U):
        Uxy = U[0:2*(self.N1+1)] + self.newpos #extract only x and y displacement from state vector
        pos_x = Uxy[0:self.N1+1] 
        pos_y = Uxy[self.N1+1:]  

        #EXTERNAL LAYER OF SPRINGS =======================================================================
        dext0 = np.sqrt(np.power(pos_x[0]-pos_x[2], 2) + np.power(pos_y[0]-pos_y[2], 2))   #current segment length
        dext1 = np.sqrt(np.power(pos_x[1]-pos_x[2], 2) + np.power(pos_y[1]-pos_y[2], 2))   #current segment length
        dext2 = np.sqrt(np.power(pos_x[3]-pos_x[2], 2) + np.power(pos_y[3]-pos_y[2], 2))   #current segment length
        
        kext_i0 = -(1-self.l0[0]/dext0)*self.kext
        kext_i1 = -(1-self.l0[1]/dext1)*self.kext
        kext_i2 = -(1-self.l0[2]/dext2)*self.kext

        #Calculate external layer spring forces
        Fspring = np.zeros((2*(self.N1+1)))

        Fspring[0] = - kext_i0*(pos_x[2]-pos_x[0]) - kext_i1*(pos_x[2]-pos_x[1]) -kext_i2*(pos_x[2]-pos_x[3]) #x0
        Fspring[2] = - kext_i0*(pos_x[0]-pos_x[2]) - kext_i1*(pos_x[1]-pos_x[2]) - kext_i2*(pos_x[3]-pos_x[2])  #x2

        Fspring[4] = - kext_i0*(pos_y[2]-pos_y[0]) - kext_i1*(pos_y[2]-pos_y[1]) - kext_i2*(pos_y[2]-pos_y[3]) #y0
        Fspring[6] = - kext_i0*(pos_y[0]-pos_y[2]) - kext_i1*(pos_y[1]-pos_y[2]) - kext_i2*(pos_y[3]-pos_y[2]) #y2

        # Split forces for plotting
        PlotFspring = np.zeros((2*(self.N1+1)))
        PlotFspring[0] = -kext_i0*(pos_x[2]-pos_x[0])  #x0
        PlotFspring[1] = -kext_i1*(pos_x[2]-pos_x[1])  #x1
        PlotFspring[3] = -kext_i2*(pos_x[2]-pos_x[3])  #x3
        PlotFspring[2] = -kext_i0*(pos_x[0]-pos_x[2])

        PlotFspring[4] = -kext_i0*(pos_y[2]-pos_y[0]) #y0
        PlotFspring[5] = -kext_i1*(pos_y[2]-pos_y[1]) #y1
        PlotFspring[7] = -kext_i2*(pos_y[2]-pos_y[3]) #y3
        PlotFspring[6] = -kext_i0*(pos_y[0]-pos_y[2])

        return Fspring, PlotFspring

    def calculateFcontacty(self,U):
        # Contact detection
        pos = U + self.newpos #extract position from state vector
        ind = np.where(pos[self.N1+1:] <= 0) # contact < 0
        Fcy = np.zeros((self.N1+1))     #create empty force vector to fill just nodes in contact

        if ind[0].size != 0:
            Fr = self.kc * (pos[ind[0]+self.N1+1]) #Normal force (in case of contact)
            Fcy[ind[0]] = -Fr 
        
        indexvector = np.zeros(self.N1+1)
        indexvector[ind[0]] = 1
        return Fcy, ind, indexvector
     
def main():
    finger = Fingertip()
    U, Fgrip, pos,t, sFspring, sFcontact, ttotal, SM = finger.rungeKutta()

    # visualise deformation
    visual = AnimationClass(U,Fgrip,pos,t,sFspring,sFcontact,ttotal, SM)
    visual.run()

if __name__ == "__main__":
    main()