#! python3
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib import colors as mcolors
import datetime
import os
import numpy as np

class AnimationClass: 
    def __init__(self,U,Fgrip,pos,t,sFspring,sFcontact,ttotal, SM):
        self.N1 = 51
        self.U = U
        self.Fgrip = Fgrip
        self.sFspring = sFspring
        self.sFcontact = sFcontact
        self.pos = pos
        self.framesavail = t
        self.ttotal = ttotal
        self.SM = SM

        self.fig = plt.figure(figsize=(10,8)) 
        self.ax = plt.axes(xlim =(-0.01,0.015), ylim =(-0.002, 0.01))
        self.ax.hlines(0,-0.01,0.01, linewidth=0.5,color='k')
        self.line, = self.ax.plot([], [] , marker='o',markersize=2, color='grey', linestyle='-', linewidth=2)
        self.texts = self.ax.text(0,0.009,'', fontsize=15)

        self.ax.grid()
        self.fig.suptitle('Motion')
        plt.xlabel('Distance (m)')
        plt.ylabel('Distance (m)')

    def animate(self,i):
        tt = 300
        self.line.set_ydata(self.pos[self.N1+1:,tt*i])
        self.line.set_xdata(self.pos[0:self.N1+1,tt*i]) # Update position data

        tstamp = "{:.4f}".format(self.ttotal[i*tt])
        self.texts.set_text('Time = %s sec'%(tstamp))   # Display current time

        return self.line, self.texts

    def run(self):
        # self.plotjes()
        self.threeplotjes()
        anim = animation.FuncAnimation(self.fig, self.animate, blit=True, frames = int(np.floor(self.framesavail/300)), repeat=True)
        plt.show()
        a = 1

    def threeplotjes(self):
        mid = int(np.ceil(self.N1/2))
        plt.figure()
        plt.title('CONTACT FORCES friction and normal')
        plt.plot(self.ttotal, np.sum(self.sFcontact[2:self.N1,:],axis=0),label='sum x', marker='.')
        plt.plot(self.ttotal, np.sum(self.sFcontact[self.N1+3:-1,:], axis=0),label='sum y', marker='.')
        plt.plot(self.ttotal, self.sFcontact[mid,:],label='mid x', marker='.')
        plt.plot(self.ttotal, self.sFcontact[mid-1,:],label='left x', marker='.')
        plt.plot(self.ttotal, self.sFcontact[mid+1,:],label='right x', marker='.')
        plt.plot(self.ttotal, self.sFcontact[mid+self.N1+1,:],label='mid y', marker='.')
        plt.plot(self.ttotal, self.sFcontact[mid-1+self.N1+1,:],label='left y', marker='.')
        plt.plot(self.ttotal, self.sFcontact[mid+1+self.N1+1,:],label='right y', marker='.')
        plt.legend()
        plt.grid()

        # plt.figure()
        # plt.title('SPRING FORCES ')
        # plt.plot(self.ttotal, self.sFspring[0,:],label='x0', marker='.')
        # plt.plot(self.ttotal, self.sFspring[1,:],label='x1', marker='.')
        # plt.plot(self.ttotal, self.sFspring[2,:],label='x2', marker='.')
        # plt.plot(self.ttotal, self.sFspring[3,:],label='x3', marker='.')
        # plt.plot(self.ttotal, self.sFspring[4,:],label='y0', marker='.')
        # plt.plot(self.ttotal, self.sFspring[5,:],label='y1', marker='.')
        # plt.plot(self.ttotal, self.sFspring[6,:],label='y2', marker='.')
        # plt.plot(self.ttotal, self.sFspring[7,:],label='y3', marker='.')
        # plt.legend()
        # plt.grid()

        plt.figure()
        plt.title('GRIP FORCES ')
        plt.plot(self.ttotal, self.Fgrip[0,:],label='x0', marker='.')
        plt.plot(self.ttotal, self.Fgrip[self.N1+1,:],label='y0', marker='.')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('SAFETY MARGIN')
        plt.plot(self.ttotal, self.SM , marker='.')
        plt.grid()

    def plotjes(self):
        # mid = int(np.ceil(self.N1/2))
        plt.figure()
        plt.title('CONTACT FORCES friction and normal')
        plt.plot(self.ttotal, self.sFcontact[0,:],label='x0', marker='.')
        plt.plot(self.ttotal, self.sFcontact[1,:],label='x1', marker='.')
        plt.plot(self.ttotal, self.sFcontact[2,:],label='y0', marker='.')
        plt.plot(self.ttotal, self.sFcontact[3,:],label='y1', marker='.')
        # plt.plot(self.ttotal, np.sum(self.sFcontact[self.N1+1:,0:t],axis=0),label='SUM y', marker='.')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('SPRING FORCES ')
        plt.plot(self.ttotal, self.sFspring[0,:],label='x0', marker='.')
        plt.plot(self.ttotal, self.sFspring[1,:],label='x1', marker='.')
        plt.plot(self.ttotal, self.sFspring[2,:],label='y0', marker='.')
        plt.plot(self.ttotal, self.sFspring[3,:],label='y1', marker='.')
        # plt.plot(self.ttotal, np.sum(self.sFspring[self.N1+1:,0:t],axis=0),label='SUM y', marker='.')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('GRIP FORCES ')
        plt.plot(self.ttotal, self.Fgrip[0,:],label='x0', marker='.')
        plt.plot(self.ttotal, self.Fgrip[1,:],label='x1', marker='.')
        plt.plot(self.ttotal, self.Fgrip[2,:],label='y0', marker='.')
        plt.plot(self.ttotal, self.Fgrip[3,:],label='y1', marker='.')
        plt.legend()
        plt.grid()