#! python3
import matplotlib.pyplot as plt
import numpy as np


class PlotClass:
    def __init__(self,U,Fgrip,pos,t,sFext,sFspring,sFcontact,sFSPext,sFSPint,sFSPn,SM):
        self.N1 = 11
        self.r = 8e-3

        self.plotjes(U,pos,Fgrip,t,sFext,sFspring,sFcontact,sFSPext,sFSPint,sFSPn,SM)
        

    def plotjes(self,U,pos,Fgrip,t,sFext,sFspring,sFcontact,sFSPext,sFSPint,sFSPn,SM):
        #add bone for drawing clarity
        leftbonex = pos[0,:].copy() - self.r
        rightbonex = pos[0,:].copy() + self.r
        boney = pos[self.N1+1,:].copy()

        pos_bonex = pos[1:self.N1+1,:].copy()        #split position in x and y and exclude bone
        pos_boney = pos[self.N1+2:,:].copy()
        #add new bone
        pos_bonex = np.insert(pos_bonex,0,leftbonex,axis=0)
        pos_bonex = np.append(pos_bonex,[rightbonex],axis=0)
        pos_boney = np.insert(pos_boney,0,boney,axis=0)
        pos_boney = np.append(pos_boney,[boney],axis=0)

        pos_complete = np.append(pos_bonex,pos_boney,axis=0)

        plt.figure()
        for i in range(int(t/100)): 
            j = i*100
            pos_x = pos_complete[0:self.N1+2,j]
            pos_y = pos_complete[self.N1+2:,j]
            plt.plot(pos_x, pos_y,label='time = %i' %j, marker='o')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('Safety margin')
        plt.plot(SM)
        plt.xlabel('Time')
        plt.ylabel('Safety Margin (%)')
        plt.grid()

        plt.figure()
        plt.title('GRIP FORCE')
        # plt.plot(Fgrip[0,:],label='Fx 0', marker='.')
        # plt.plot(Fgrip[1,:],label='Fx 1', marker='.')
        # plt.plot(Fgrip[self.N1,:],label='Fx N', marker='.')
        plt.plot(Fgrip[self.N1+1,:],label='Fy 0', marker='.',linewidth=3)
        plt.plot(Fgrip[self.N1+2,:],label='Fy 1', marker='.',linewidth=2)
        plt.plot(Fgrip[-1,:],label='Fy N', marker='.',linewidth=1)
        plt.legend()
        plt.grid()

        mid = int(np.ceil(self.N1/2))
        plt.figure()
        plt.title('CONTACT FORCES friction and normal')
        plt.plot(sFcontact[mid,:],label='Middel node x', marker='.')
        plt.plot(sFcontact[mid+1,:],label='Right of mid x', marker='.')
        plt.plot(sFcontact[mid-1,:],label='Left of mid x', marker='.')
        # plt.plot(sFcontact[mid+2,:],label='2 Right', marker='.')
        # plt.plot(sFcontact[mid-2,:],label='2 Left', marker='.')
        # plt.plot(sFcontact[self.N1+1+mid,:],label='Middel node y', marker='.')
        # plt.plot(sFcontact[self.N1+1+mid+1,:],label='Right of mid y', marker='.')
        # plt.plot(sFcontact[self.N1+1+mid-1,:],label='Left of mid y', marker='.')
        # plt.plot(np.sum(sFcontact[1:self.N1+1,0:t],axis=0),label='SUM x', marker='.')
        # plt.plot(np.sum(sFcontact[self.N1+2:,0:t],axis=0),label='SUM y', marker='.')
        plt.legend()
        plt.grid()

        # plt.figure()
        # plt.title('EXTERNAL FORCES (contact + grip)')
        # plt.plot(sFext[0,:],label='Fx 0', marker='.')
        # plt.plot(sFext[mid,:],label='Middel node x', marker='.')
        # plt.plot(sFext[mid+1,:],label='Right of mid x', marker='.')
        # plt.plot(sFext[mid-1,:],label='Left of mid x', marker='.')
        # plt.plot(sFext[self.N1+1,:],label='Fy 0', marker='.')
        # plt.plot(sFext[self.N1+1+mid,:],label='Middel node y', marker='.')
        # plt.plot(sFext[self.N1+1+mid+1,:],label='Right of mid y', marker='.')
        # plt.plot(sFext[self.N1+1+mid-1,:],label='Left of mid y', marker='.')
        # plt.plot(np.sum(sFext[1:self.N1+1,0:t],axis=0),label='SUM x', marker='.')
        # plt.plot(np.sum(sFext[self.N1+2:,0:t],axis=0),label='SUM y', marker='.')
        # plt.legend()
        # plt.grid()

        # plt.figure()
        # plt.title('HORIZONTAL DISPLACEMENT at contact')
        # plt.plot(U[mid,:],label='Middel node x', marker='.')
        # plt.plot(U[mid+1,:],label='1Right', marker='.')
        # plt.plot(U[mid-1,:],label='1Left', marker='.',linestyle='-')
        # plt.plot(U[mid+2,:],label='2Right', marker='.')
        # plt.plot(U[mid-2,:],label='2Left', marker='.',linestyle='-')
        # plt.legend()
        # plt.grid()


        # plt.figure()
        # plt.title('TOTAL SPRING FORCES')
        # # plt.plot(self.sFspring[0,:],label='Fx 0', marker='.')
        # # plt.plot(self.sFspring[1,:],label='Fx 1', marker='.')
        # # plt.plot(self.sFspring[2,:],label='Fx 2', marker='.')
        # # plt.plot(self.sFspring[self.N1,:],label='Fx N', marker='.')
        # plt.plot(sFspring[self.N1+1,:],label='Fy 0', marker='.',linewidth=3)
        # plt.plot(sFspring[self.N1+2,:],label='Fy 1', marker='.',linewidth=2)
        # plt.plot(sFspring[-1,:],label='Fy N', marker='.',linewidth=1)
        # plt.legend()
        # plt.grid()

        plt.figure()
        plt.title('SPRING FORCE Y bone connection')
        plt.plot(sFSPn[self.N1+1,:],label='Fy 0', marker='.')
        plt.plot(sFSPn[self.N1+2,:],label='Fy 1', marker='.')
        plt.plot(sFSPn[-1,:],label='Fy N', marker='.')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('SPRING FORCE external x direction')
        plt.plot(sFSPext[0,:],label='Fx 0', marker='.')
        plt.plot(sFSPext[1,:],label='Fx 1', marker='.')
        # plt.plot(sFSPext[2,:],label='Fx 2', marker='.')
        # plt.plot(sFSPext[3,:],label='Fx 3', marker='.')
        # plt.plot(sFSPext[4,:],label='Fx 4', marker='.')
        # plt.plot(sFSPext[5,:],label='Fx 5', marker='.')
        # plt.plot(sFSPext[6,:],label='Fx 6', marker='.')
        # plt.plot(sFSPext[7,:],label='Fx 7', marker='.')
        # plt.plot(sFSPext[8,:],label='Fx 8', marker='.')
        # plt.plot(sFSPext[9,:],label='Fx 9', marker='.')
        plt.plot(sFSPext[10,:],label='Fx 10', marker='.')
        plt.plot(sFSPext[self.N1,:],label='Fx N', marker='.')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('SPRING FORCE external Y direction')
        plt.plot(sFSPext[0+self.N1+1,:],label='Fy 0', marker='.')
        plt.plot(sFSPext[1+self.N1+1,:],label='Fy 1', marker='.')
        # plt.plot(sFSPext[2+self.N1+1,:],label='Fy 2', marker='.')
        # plt.plot(sFSPext[3+self.N1+1,:],label='Fy 3', marker='.')
        # plt.plot(sFSPext[4+self.N1+1,:],label='Fy 4', marker='.')
        # plt.plot(sFSPext[5+self.N1+1,:],label='Fy 5', marker='.')
        # plt.plot(sFSPext[6+self.N1+1,:],label='Fy 6', marker='.')
        # plt.plot(sFSPext[7+self.N1+1,:],label='Fy 7', marker='.')
        # plt.plot(sFSPext[8+self.N1+1,:],label='Fy 8', marker='.')
        # plt.plot(sFSPext[9+self.N1+1,:],label='Fy 9', marker='.')
        plt.plot(sFSPext[10+self.N1+1,:],label='Fy 10', marker='.')
        plt.plot(sFSPext[11+self.N1+1,:],label='Fy N', marker='.')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('SPRING FORCE internal x direction')
        plt.plot(sFSPint[0,:],label='Fx 0', marker='.')
        # plt.plot(sFSPint[1,:],label='Fx 1', marker='.')
        # plt.plot(sFSPint[2,:],label='Fx 2', marker='.')
        # plt.plot(sFSPint[3,:],label='Fx 3', marker='.')
        # plt.plot(sFSPint[4,:],label='Fx 4', marker='.')
        plt.plot(sFSPint[5,:],label='Fx 5', marker='.')
        plt.plot(sFSPint[6,:],label='Fx 6', marker='.')
        plt.plot(sFSPint[7,:],label='Fx 7', marker='.')
        # plt.plot(sFSPint[8,:],label='Fx 8', marker='.')
        # plt.plot(sFSPint[9,:],label='Fx 9', marker='.')
        # plt.plot(sFSPint[10,:],label='Fx 10', marker='.')
        # plt.plot(sFSPint[self.N1,:],label='Fx N', marker='.')
        plt.legend()
        plt.grid()

        plt.figure()
        plt.title('SPRING FORCE internal Y direction')
        plt.plot(sFSPint[0+self.N1+1,:],label='Fy 0', marker='.')
        # plt.plot(sFSPint[1+self.N1+1,:],label='Fy 1', marker='.')
        # plt.plot(sFSPint[2+self.N1+1,:],label='Fy 2', marker='.')
        # plt.plot(sFSPint[3+self.N1+1,:],label='Fy 3', marker='.')
        # plt.plot(sFSPint[4+self.N1+1,:],label='Fy 4', marker='.')
        plt.plot(sFSPint[5+self.N1+1,:],label='Fy 5', marker='.')
        plt.plot(sFSPint[6+self.N1+1,:],label='Fy 6', marker='.')
        plt.plot(sFSPint[7+self.N1+1,:],label='Fy 7', marker='.')
        # plt.plot(sFSPint[8+self.N1+1,:],label='Fy 8', marker='.')
        # plt.plot(sFSPint[9+self.N1+1,:],label='Fy 9', marker='.')
        # plt.plot(sFSPint[10+self.N1+1,:],label='Fy 10', marker='.')
        # plt.plot(sFSPint[11+self.N1+1,:],label='Fy N', marker='.')
        plt.legend()
        plt.grid()

        plt.show()
        a=1