""" This file contains a variant of Michael's code."""

import numpy as np
import matplotlib.pyplot as plt
import math
from animate import AnimationClass
import sys

class Fingertip:
    def __init__(self):
        # skin structure
        self.r = 8e-3               # finger radius (m)
        self.N1 = 151               # number of nodes (-)
        self.d0 = 1e-5              # initial height sensor (m)

        self.sigma0 = 1e4;          # rest stiffness of the bristle (N/m)
        self.n = 0.7                # exponent: material dependent parameter
        self.fclb = 0.2             # coulomb friction coefficient

        self.Fs = 1e5               # samping frequency (Hz)

        # external forces applied on the bone
        self.P = -0.1                # initial pressure (N)       
        self.Q = 0.1                 # initial pressure (N)       

        # initial parameters
        theta1 = np.linspace(-np.pi,0,self.N1+2)    #semi circle (rad)
        theta1r = theta1[::-1]                      #Very cumbersome way to obtain symmetric angles
        theta2 = np.append(-np.pi/2,-(theta1r[math.ceil(self.N1/2)+1:self.N1+1]+np.pi))
        theta = np.concatenate((theta1[0:math.ceil(self.N1/2)],theta2,[0]))

        self.x, self.y = self.pol2cart(theta[1:-1], self.r*np.ones(theta[1:-1].size))   #polar coordinates to cartesian
        self.y += self.d0+self.r 
        self.newpos = np.concatenate(([0],self.x,[self.d0+self.r], self.y))         #[xb x1 x2 .. xn yb y1 y2 ... yn]

        self.dSkin = np.sqrt(np.power(np.diff(self.y),2)+np.power(np.diff(self.x),2))    #length of each membrane segment
        
        self.l0 = np.mean(self.dSkin)                                                    #average segment length
        self.l0b = self.r

        #Damping and spring stiffnesses, TODO: check with different values 
        self.kc = 1e2 #1e3          # Stiffness surface (N/m)
        self.kext = 20 #100         # Stiffness external springs (N/m)
        self.kint = 20 #10          # Stiffness internal springs (N/m)
        self.kn = 20  #200            # Stiffness springs connecting external springs to bone (N/m)
        self.c = 1                  # damping ratio (-)

        self.karray = np.array([self.kext,self.kint,self.kn])

    def pol2cart(self, phi, rho):  
        """ convert polar coordinates to cartesian
            phi = angle (rad), rho = radius (m) 
            x,z in (m)
            """
        x = rho * np.cos(phi)
        z = rho * np.sin(phi)
        return(x, z)

    def rungeKutta(self):
        #Time vector
        dt = 1/self.Fs                   #sampling period (sec)
        timev = np.arange(0,0.5+dt,dt)   #time vector (sec)
        t = 1 
        
        timeconstant = self.c/np.max(self.karray)
        if dt >= timeconstant:
            print("Check ration c/k. Timestep is too large. Should be lower than %s" % timeconstant)
            sys.exit()

        #Initial force and diplacement matrices 
        U = np.zeros((3*(self.N1+1),timev.size))            #state vector U = [Ux, Uy, Fxc]
        Fgrip = np.zeros((2*(self.N1+1),timev.size))        #grip force F = [Fx, Fy]
        Fgrip[self.N1+1,:] = self.P 
        # Fgrip[0,:] = self.Q 
    
        #save forces independently
        self.sFspring   = np.zeros((2*(self.N1+1),timev.size))      #internal spring forces F = [Fx, Fy]
        self.sFcontact  = np.zeros((2*(self.N1+1),timev.size))      #external contact forces F = [Fx, Fy]
        self.sFext      = np.zeros((2*(self.N1+1),timev.size))      #total external forces F = [Fx, Fy]

        self.sFSPn     = np.zeros((2*(self.N1+1),timev.size))        #internal spring forces CONNECTION TO BONE
        self.sFSPext   = np.zeros((2*(self.N1+1),timev.size))      #internal spring forces EXTERNAL LAYER
        self.sFSPint   = np.zeros((2*(self.N1+1),timev.size))      #internal spring forces INTERNAL STRUCTURE

        self.sIndex = np.zeros((self.N1+1,timev.size))
          
        while t<timev.size: 
            # Calculate displacement (Runge Kutta 4th order)                
            k1 = self.dydx(U[:,t-1], Fgrip[:,t-1],t)
            k2 = self.dydx(U[:,t-1] + 0.5*dt*k1, Fgrip[:,t-1],t)
            k3 = self.dydx(U[:,t-1] + 0.5*dt*k2, Fgrip[:,t-1],t)
            k4 = self.dydx(U[:,t-1] + dt*k3, Fgrip[:,t-1],t) 

            dU = (1 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)
    
            # Update displacement
            U[:,t] = U[:,t-1] + dt*dU

            t=t+1 
        ti=t
        # Fgrip[ti,:] = self.Q #Apply force sideways 
        # while t < 2000:
        #     # Calculate displacement (Runge Kutta 4th order)                
        #     k1 = self.dydx(U[:,t-1], Fgrip[:,t-1],t)
        #     k2 = self.dydx(U[:,t-1] + 0.5*dt*k1, Fgrip[:,t-1],t)
        #     k3 = self.dydx(U[:,t-1] + 0.5*dt*k2, Fgrip[:,t-1],t)
        #     k4 = self.dydx(U[:,t-1] + dt*k3, Fgrip[:,t-1],t) 

        #     dU = (1 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)
    
        #     # Update displacement
        #     U[:,t] = U[:,t-1] + dt*dU

        #     t=t+1 
        
        U = U[:,0:t]
        pos = U[0:2*(self.N1+1)] + self.newpos[:,None]

        SM = (self.fclb*self.P - np.sum(self.sFcontact[1:self.N1+1,0:t],axis=0))/(self.fclb*self.P)

        Fgrip = Fgrip[:,0:t]
        self.sFext = self.sFext[:,0:t]
        self.sFspring = self.sFspring[:,0:t]
        self.sFcontact = self.sFcontact[:,0:t]
        self.sFSPext = self.sFSPext[:,0:t]
        self.sFSPint = self.sFSPint[:,0:t]
        self.sFSPn = self.sFSPn[:,0:t]
        self.sIndex = self.sIndex[:,0:t]

        return U, Fgrip, pos,t, self.sFext,self.sFspring,self.sFcontact,self.sFSPext,self.sFSPint,self.sFSPn, SM, self.sIndex

    def dydx(self, U , Fgrip,t): 
        Uxy = U[0:2*(self.N1+1)]
        
        # Spring forces
        Fspring = self.calculateFspring(Uxy,t)   
        self.sFspring[:,t-1] = Fspring.copy() 

        # Contact forces
        Fxc = U[2*(self.N1+1):]
        Fyc, ind = self.calculateFcontacty(Uxy,t) #contact force in y direction and contact indices at time t
        self.sFcontact[self.N1+1:,t-1] = Fyc.copy() 
        self.sFcontact[0:self.N1+1,t-1] = Fxc.copy() 

        # Total external forces
        Fext = np.concatenate((np.zeros(self.N1+1),Fyc)) + np.concatenate((Fxc, np.zeros(self.N1+1))) + Fgrip #Fcontact + Fgrip
        self.sFext[:,t-1] = Fext.copy() 

        dUxy = Fspring/self.c + Fext/self.c
        
        # Friction (case of contact)
        dUx = dUxy[0:self.N1+1] 
        dUFxc = np.zeros(self.N1+1)

        if ind[0].size != 0:
            dF1 = self.sigma0*dUx[ind[0]+2] #ind[0] + 2, bc bone is excluded
            dF2 = np.power(np.abs(1+np.divide(Fxc[ind[0]+2],self.fclb*Fyc[ind[0]+2])*np.sign(dUx[ind[0]+2])),self.n) 
            dFsigns = np.sign(1+np.divide(Fxc[ind[0]+2],self.fclb*Fyc[ind[0]+2])*np.sign(dUx[ind[0]+2]))
            dUFxc[ind[0]+2] = - dF1*dF2*dFsigns
            
        dU = np.concatenate((dUxy,dUFxc))
        return dU

    def calculateFspring(self,U,t):
        Uxy = U[0:2*(self.N1+1)] + self.newpos #extract only x and y displacement from state vector
        pos_x = Uxy[0:self.N1+1] 
        pos_y = Uxy[self.N1+1:]  
        pos_complete = Uxy 

        #EXTERNAL LAYER OF SPRINGS =======================================================================
        dext = np.sqrt(np.power(np.diff(pos_x[1:]),2) + np.power(np.diff(pos_y[1:]),2))   #current segment length
        kext_i = -(1-self.l0/dext)*self.kext

        #Calculate external layer spring forces
        Fspring_ext = np.zeros((2*(self.N1+1)))

        entry1 = - np.append(kext_i,0)
        entry2 = - np.insert(kext_i,0,0)
        entrymid = - entry1 - entry2

        Fspring_ext[1:self.N1+1] = np.multiply(entry2,pos_x[0:-1]) + np.multiply(entrymid,pos_x[1:]) + np.multiply(entry1,np.append(pos_x[2:],0))
        Fspring_ext[self.N1+2:] = np.multiply(entry2,pos_y[0:-1]) + np.multiply(entrymid,pos_y[1:]) + np.multiply(entry1,np.append(pos_y[2:],0))
      
        self.sFSPext[:,t] = Fspring_ext.copy() 

        #INTERNAL STRUCTURE OF SPRINGS =======================================================================
        dint = np.sqrt(np.power(pos_x[1:] - pos_x[0],2) + np.power(pos_y[1:] - pos_y[0],2))   #current segment length  
        kint_i = -(1-self.l0b/dint)*self.kint 

        #Calculate internal structure spring forces
        Fspring_int = np.zeros((2*(self.N1+1)))
        
        #BCs 
        Fspring_int[0] = np.dot(np.sum(kint_i),pos_x[0]) +  np.dot(-kint_i,pos_x[1:]) 
        Fspring_int[self.N1+1] = np.dot(np.sum(kint_i),pos_complete[self.N1+1]) + np.dot(-kint_i,pos_complete[self.N1+2:])
        
        #General equation
        Fspring_int[1:self.N1+1] = np.multiply(kint_i,pos_complete[1:self.N1+1]) + np.multiply(-kint_i,pos_complete[0]) 
        Fspring_int[self.N1+2:] = np.multiply(kint_i,pos_complete[self.N1+2:]) + np.multiply(-kint_i,pos_complete[self.N1+1]) 

        self.sFSPint[:,t] = Fspring_int.copy()
        
        #CONNECTION TO BONE SPRINGS =======================================================================
        kn_i = self.kn
        Fspring_n = np.zeros((2*(self.N1+1)))
        Fspring_n[self.N1+1] = -2*kn_i*pos_complete[self.N1+1]+kn_i*pos_complete[self.N1+2]+kn_i*pos_complete[-1]   #force on bone
        Fspring_n[self.N1+2] = kn_i*pos_complete[self.N1+1] - kn_i*pos_complete[self.N1+2]                          #force on node 1
        Fspring_n[-1] = kn_i*pos_complete[self.N1+1] - kn_i*pos_complete[-1]                                        #force on node N

        self.sFSPn[:,t] = Fspring_n.copy()

        # SUM ALL SPRING FORCES =======================================================================
        Fspring = Fspring_ext + Fspring_int + Fspring_n

        return Fspring

    def calculateFcontacty(self,U,t):
        # Contact detection
        pos = U + self.newpos #extract position from state vector

        ind = np.where(pos[self.N1+3:-1] <= 0) #exclude bone, node 1 & node N

        Fcy = np.zeros((self.N1+1))     #create empty force vector to fill just nodes in contact
        self.sIndex[ind[0]+2,t-1] = 1

        if ind[0].size != 0:
            Fr = self.kc * (pos[ind[0]+self.N1+3]) #Normal force (in case of contact)
            Fcy[ind[0]+2] = -Fr 
        return Fcy, ind
     
def main():
    finger = Fingertip()
    U, Fgrip, pos,t,sFext,sFspring,sFcontact,sFSPext,sFSPint,sFSPn,SM, sIndex = finger.rungeKutta()

    F = sFspring+sFSPext+sFSPint+sFSPn+Fgrip
    dt = 1e-05

    # visualise deformation
    visual = AnimationClass(U,Fgrip,pos,t,sFext,sFspring,sFcontact,sFSPext,sFSPint,sFSPn,SM, sIndex, save=False)
    visual.run()
    
    # visual = PlotClass(U,Fgrip,pos,t,sFext,sFspring,sFcontact,sFSPext,sFSPint,sFSPn, SM)


if __name__ == "__main__":
    main()