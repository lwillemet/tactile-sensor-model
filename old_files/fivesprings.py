#! python3
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from animatesimple import AnimationClass

class Fingertip:
    def __init__(self):
        # sensor structure
        self.r = 8e-3               # radius radius in m
        self.N1 = 5               # number of nodes
        self.d0 = 1e-5              # Initial height of center in m

        self.sigma0 = 1e4;          # rest stiffness of the bristle (N/m)       
        self.n = 1 #0.7               # exponent: material dependent parameter
        self.fclb = 0.2             # coulomb friction coefficient

        self.Fs = 1e5               # samping frequency (Hz)

        # External forces applied on the bone
        self.P = -0.15               # initial pressure (N)       
        self.Q = 0.04 #0.1          # initial pressure (N)       

        # initial parameters
        self.x = np.array([0, -self.r, -3/4*self.r, 0, 3/4*self.r, self.r])
        self.y = np.array([self.r+self.d0, self.r+self.d0,0.5*(self.r+self.d0) ,self.d0, 0.5*(self.r+self.d0), self.r+self.d0])   
        self.newpos = np.concatenate((self.x, self.y))         #[xb x1 x2 .. xn yb y1 y2 ... yn]

        #internal spring lengths
        self.l0_int = np.sqrt(np.power(self.x[0]-self.x[2:self.N1], 2) + np.power(self.y[0]-self.y[2:self.N1], 2))

        #external spring lengths
        self.l0_ext = np.sqrt(np.power(np.diff(self.x[1:]), 2) + np.power(np.diff(self.y[1:]), 2))

        #Damping and spring stiffnesses  
        self.kc = 1e2          # Stiffness surface (N/m)
        self.kext = 100 #100        # Stiffness external springs (N/m)
        self.c = 1                  # Damping ratio (-)

    def rungeKutta(self):
        #Initial force and diplacement matrices 
        Fgrip = np.zeros(2*(self.N1+1))            #grip force vector F = [Fx, Fy]
        Fgrip[self.N1+1] = self.P                  #apply downward force on node 0
        Fgrip[0] = self.Q                        #apply sideward force on node 0

        u0 =  np.zeros(3*(self.N1+1))  #Initial values solver; u0 = [ux, uy, fcontx]

        outputu = solve_ivp(self.dydx,(0,1),u0, args=[Fgrip])
        # Fgrip[0] = self.Q
        # u02 = outputu.y[:,-1]
        # outputu2 = solve_ivp(self.dydx,(0,0.5),u02, args=[Fgrip])

        U = outputu.y[0:2*(self.N1+1),:]    #ONE SOLVER displacement per timestep 
        # U = np.concatenate((outputu.y[0:2*(self.N1+1),:],outputu2.y[0:2*(self.N1+1),:]),axis=1)    #TWO SOLVERS displacement per timestep 

        pos = U + self.newpos[:,None]       #position per timestep

        # obtain forces and indices at each timestep
        # t = outputu.t.size + outputu2.t.size    # TWO SOLVERS
        t = outputu.t.size                      # ONE SOLVER
        columsnr = t
        sFcontact = np.zeros((2*(self.N1+1),columsnr))
        sFspring = np.zeros((2*(self.N1+1),columsnr))
        sFspringext = np.zeros((2*(self.N1+1),columsnr))
        sFspringint = np.zeros((2*(self.N1+1),columsnr))
        sIndex = np.zeros((self.N1+1,columsnr))

        for ii in range(columsnr):
            sFspring[:,ii], sFspringext[:,ii], sFspringint[:,ii] = self.calculateFspring(U[:,ii]) #Fspring, Fspringext, Fspringint
            sFcontact[self.N1+1:,ii], ind, sIndex[:,ii] = self.calculateFcontacty(U[:,ii])

        sFcontact[0:self.N1+1] = outputu.y[2*(self.N1+1):,:] #ONE SOLVER
        # sFcontact[0:self.N1+1] = np.concatenate((outputu.y[2*(self.N1+1):,:],outputu2.y[2*(self.N1+1):,:]),axis=1) #TWO SOLVERS

        Fgrip = np.zeros((2*(self.N1+1),columsnr))
        Fgrip[0,:] =self.Q
        Fgrip[self.N1+1,:] =self.P

        # ttotal = np.concatenate((outputu.t, outputu2.t+outputu.t[-1] )) #TWO SOLVERS
        ttotal = outputu.t     #ONE SOLVER

        # Safety Margin 
        SM = (self.fclb*self.P - np.sum(sFcontact[2:self.N1,:], axis=0))/(self.fclb*self.P) #calculate safety margin

        return U, Fgrip, pos,t, sFspring, sFcontact, ttotal , SM

    def dydx(self, t, U , Fgrip): 
        Uxy = U[0:2*(self.N1+1)]
        Fxc = U[2*(self.N1+1):3*(self.N1+1)]

        # Spring forces
        Fspring, Fspringext, Fspringint = self.calculateFspring(Uxy)  

        # Contact forces
        Fyc, ind, indexvector = self.calculateFcontacty(Uxy) #contact force in y direction and contact indices at time t

        # Total external forces
        Fext = np.concatenate((np.zeros(self.N1+1),Fyc)) + np.concatenate((Fxc, np.zeros(self.N1+1))) + Fgrip #Fcontact + Fgrip
        
        #Differential equation for dUx and dUy
        dUxy = Fspring/self.c + Fext/self.c
        
        # Friction (case of contact)
        dUx = dUxy[0:self.N1+1] 
        dUFxc = np.zeros(self.N1+1)

        if ind[0].size != 0: #Differential equation for dFxcontact
            dF1 = self.sigma0*dUx[ind[0]] 
            dF2 = np.power(np.abs(1+np.divide(Fxc[ind[0]],self.fclb*Fyc[ind[0]])*np.sign(dUx[ind[0]])),self.n) 
            dFsigns = np.sign(1+np.divide(Fxc[ind[0]],self.fclb*Fyc[ind[0]])*np.sign(dUx[ind[0]]))
            dUFxc[ind[0]] = - dF1*dF2*dFsigns
            
        dU = np.concatenate((dUxy,dUFxc))

        # Constraints
        dU[1] = dU[self.N1] = dU[0] #x
        dU[self.N1+2] = dU[2*(self.N1+1)-1] = dU[self.N1+1] #y
        return dU

    def calculateFspring(self,U):
        Uxy = U[0:2*(self.N1+1)] + self.newpos #extract only x and y displacement from state vector
        pos_x = Uxy[0:self.N1+1] 
        pos_y = Uxy[self.N1+1:]  

        #EXTERNAL LAYER OF SPRINGS =======================================================================
        dext = np.sqrt(np.power(np.diff(pos_x[1:]), 2) + np.power(np.diff(pos_y[1:]), 2)) #current segment length

        kext_i = -(1-self.l0_ext/dext)*self.kext

        #Calculate external layer spring forces
        Fspringext = np.zeros((2*(self.N1+1)))

        # Fspringext[0] = - kext_i1*(pos_x[2]-pos_x[1]) - kext_i4*(pos_x[4]-pos_x[5]) #x0
        # Fspringext[6] = - kext_i1*(pos_y[2]-pos_y[1]) - kext_i4*(pos_y[4]-pos_y[5]) #y0
        #Boundary springs
        Fspringext[0] = - kext_i[0]*(pos_x[2]-pos_x[1]) - kext_i[self.N1-2]*(pos_x[self.N1-1]-pos_x[self.N1]) #x0
        Fspringext[self.N1+1] = - kext_i[0]*(pos_y[2]-pos_y[1]) - kext_i[self.N1-2]*(pos_y[self.N1-1]-pos_y[self.N1]) #y0

        #General external
        # Fspringext[2] = - kext_i1*(pos_x[1]-pos_x[2]) - kext_i2*(pos_x[3]-pos_x[2]) #x
        # Fspringext[3] = - kext_i2*(pos_x[2]-pos_x[3]) - kext_i3*(pos_x[4]-pos_x[3])
        # Fspringext[4] = - kext_i3*(pos_x[3]-pos_x[4]) - kext_i4*(pos_x[5]-pos_x[4])

        Fspringext[2:self.N1] = - np.multiply(kext_i[0:-1], -np.diff(pos_x[1:self.N1])) - np.multiply(kext_i[1:], np.diff(pos_x[2:]))
        
        # Fspringext[8] = - kext_i1*(pos_y[1]-pos_y[2]) - kext_i2*(pos_y[3]-pos_y[2]) #y
        # Fspringext[9] = - kext_i2*(pos_y[2]-pos_y[3]) - kext_i3*(pos_y[4]-pos_y[3])
        # Fspringext[10] = -kext_i3*(pos_y[3]-pos_y[4]) - kext_i4*(pos_y[5]-pos_y[4])

        Fspringext[self.N1+3:-1] = - np.multiply(kext_i[0:-1], -np.diff(pos_y[1:self.N1])) - np.multiply(kext_i[1:], np.diff(pos_y[2:]))

        # Split forces for plotting (external springs in node 1 & 5 instead of 0)
        PlotFspringext = np.zeros((2*(self.N1+1)))

        PlotFspringext[1] = - kext_i[0]*(pos_x[2]-pos_x[1]) #node 1 x
        PlotFspringext[self.N1] = - kext_i[self.N1-2]*(pos_x[self.N1-1]-pos_x[self.N1]) #node N x
        PlotFspringext[self.N1+2] = - kext_i[0]*(pos_y[2]-pos_y[1]) 
        PlotFspringext[-1] = - kext_i[self.N1-2]*(pos_y[self.N1-1]-pos_y[self.N1])
        PlotFspringext[2:self.N1] = Fspringext[2:self.N1] #same as real forces
        PlotFspringext[self.N1+3:-1] = Fspringext[self.N1+3:-1]

        #INTERNAL LAYER OF SPRINGS =======================================================================
        Fspringint = np.zeros((2*(self.N1+1)))
        dint1 = np.sqrt(np.power(pos_x[0]-pos_x[2], 2) + np.power(pos_y[0]-pos_y[2], 2))
        dint2 = np.sqrt(np.power(pos_x[0]-pos_x[3], 2) + np.power(pos_y[0]-pos_y[3], 2))
        dint3 = np.sqrt(np.power(pos_x[0]-pos_x[4], 2) + np.power(pos_y[0]-pos_y[4], 2))
        dint = np.sqrt(np.power(pos_x[0]-pos_x[2:self.N1], 2) + np.power(pos_y[0]-pos_y[2:self.N1], 2))

        kint_i1 = -(1-self.l0_int[0]/dint1)*self.kext
        kint_i2 = -(1-self.l0_int[1]/dint2)*self.kext
        kint_i3 = -(1-self.l0_int[2]/dint3)*self.kext

        kint_i = -(1-self.l0_int/dint)*self.kext

        #Connection to rigid part
        # Fspringint[0] = - kint_i1*(pos_x[2]-pos_x[0]) - kint_i2*(pos_x[3]-pos_x[0])  - kint_i3*(pos_x[4]-pos_x[0]) #x0
        # Fspringint[6] = - kint_i1*(pos_y[2]-pos_y[0]) - kint_i2*(pos_y[3]-pos_y[0])  - kint_i3*(pos_y[4]-pos_y[0]) #y0

        Fspringint[0] = - np.dot(kint_i, pos_x[2:self.N1] - pos_x[0]) #x0
        Fspringint[self.N1+1] = - np.dot(kint_i, pos_y[2:self.N1] - pos_y[0]) #y0

        #General 
        # Fspringint[2] = - kint_i1*(pos_x[0]-pos_x[2])   
        # Fspringint[3] = - kint_i2*(pos_x[0]-pos_x[3])
        # Fspringint[4] = - kint_i3*(pos_x[0]-pos_x[4]) 

        Fspringint[2:self.N1] = - kint_i*(pos_x[0]-pos_x[2:self.N1]) 

        # Fspringint[8] = - kint_i1*(pos_y[0]-pos_y[2])   
        # Fspringint[9] = - kint_i2*(pos_y[0]-pos_y[3])
        # Fspringint[10] = - kint_i3*(pos_y[0]-pos_y[4]) 

        Fspringint[self.N1+3:-1] = - kint_i*(pos_y[0]-pos_y[2:self.N1]) 

        Fspring = Fspringext + Fspringint

        return Fspring, PlotFspringext, Fspringint

    def calculateFcontacty(self,U):
        # Contact detection
        pos = U + self.newpos #extract position from state vector
        ind = np.where(pos[self.N1+1:] <= 0) # contact < 0
        Fcy = np.zeros((self.N1+1))     #create empty force vector to fill just nodes in contact

        if ind[0].size != 0:
            Fr = self.kc * (pos[ind[0]+self.N1+1]) #Normal force (in case of contact)
            Fcy[ind[0]] = -Fr 
        
        indexvector = np.zeros(self.N1+1)
        indexvector[ind[0]] = 1
        return Fcy, ind, indexvector
     
def main():
    finger = Fingertip()
    U, Fgrip, pos,t, sFspring, sFcontact, ttotal, SM = finger.rungeKutta()

    # visualise deformation
    visual = AnimationClass(U,Fgrip,pos,t,sFspring,sFcontact,ttotal, SM)
    visual.run()

if __name__ == "__main__":
    main()