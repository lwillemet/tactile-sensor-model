#! python3
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from animate import AnimationClass
import sys
from timer import Timer
import datetime
import os

class SensorContact:
    def __init__(self):
        # sensor properties
        self.r = 8e-3               # Radius radius (m)
        self.N = 51                 # Number of nodes (-)
        self.d0 = 1e-5              # Initial height of center (m)
        self.sigma0 = 1e4;          # Rest stiffness of the bristle (N/m)       
        self.n = 1                  # Exponent: material dependent parameter, !arbitrary value! (-)
        self.fclb = 0.2             # Coulomb friction coefficient, !arbitrary value! (-)
        # sensor shape
        theta = np.linspace(-np.pi,0,self.N)    # Semi circle to create sensor shape (rad)
        self.x, self.y = self.pol2cart(theta[1:-1], self.r*np.ones(theta[1:-1].size))   # Polar coordinates to cartesian, exclude 1st and last elements of theta
        self.y += self.d0+self.r                                                        # Translate nodes with initial height and radius    
        self.x = np.concatenate(([0],[-self.r],self.x,[self.r]))                                # Array x coordinates nodes
        self.y = np.concatenate(([self.d0+self.r],[self.d0+self.r], self.y,[self.d0+self.r]))   # Array y coordinate nodes
        self.inipos = np.concatenate((self.x, self.y))         # Position array, pos = [xb x1 x2 .. xn yb y1 y2 ... yn], with node 1 and N located on extremities of rigid sensor part
        # applied grip force and sideways force
        self.P = np.array([-0.01])   # Initial force down on rigid sensor part (N), change to apply different force       
        self.Q = 0.003               # Initial force side on rigid sensor part (N), change to apply different force  
        # Initial spring lengths
        self.l0_int = np.sqrt(np.power(self.x[0]-self.x[2:self.N], 2) + np.power(self.y[0]-self.y[2:self.N], 2))  # initial internal spring lengths
        self.l0_ext = np.sqrt(np.power(np.diff(self.x[1:]), 2) + np.power(np.diff(self.y[1:]), 2))                  # intitial external spring lengths
        # Spring and damping coefficients
        self.kc = 1e3          # Stiffness surface (N/m), !arbitrary value!
        self.kext = 100        # Stiffness external springs (N/m), !arbitrary value!
        self.kint = 100        # Stiffness internal springs (N/m), !arbitrary value!
        self.c = 1             # Damping ratio (-), !arbitrary value!

    def pol2cart(self, phi, rho):  
        """ convert polar coordinates to cartesian
            phi = angle (rad), rho = radius (m) 
            x,z in (m)
            """
        x = rho * np.cos(phi)
        z = rho * np.sin(phi)
        return(x, z)

    def solveODE(self):
        Fgrip = np.zeros(2*(self.N+1))             # Initial grip force vector F = [Fx, Fy]
        Fgrip[self.N+1] = self.P                   # Downward force (y) on node 0 (= rigid sensor part)
        Fgrip[0] = self.Q                          # Sideways force (x) on node 0 (= rigid sensor part)
        u0 =  np.zeros(3*(self.N+1))               # Initial values solver; state u0 = [ux, uy, fcontx]

        """ ========= START ODE SOLVER ========= """
        timer_total = Timer("Total computation time")
        timer_total.start()
        safety_margins, displacements, times = self.controlFunc(Fgrip, u0) # output safety margins and output (multiple) solver(s)
        timer_total.stop()
        """ ========= END ODE SOLVER   ========= """ 

        for i in range(len(displacements)): # Loop to fill arrays with ode outputs when multiple solution, TODO: less cumbersome way to do this? Feels a bit double
            if i == 0:
                total_time = times[i]
                total_disp = displacements[i]
                total_sm = safety_margins[i]
                fgrip = np.zeros((2*(self.N+1),len(times[i])))
                fgrip[0,:] = self.Q
                fgrip[self.N+1,:] = self.P[i]
            else: 
                total_disp = np.append(total_disp, displacements[i], axis=1)
                total_time = np.append(total_time, times[i]+total_time[-1])
                total_sm = np.append(total_sm, safety_margins[i])
                fgripadd = np.zeros((2*(self.N+1),len(times[i])))
                fgripadd[0,:] = self.Q
                fgripadd[self.N+1,:] = self.P[i]
                fgrip = np.append(fgrip, fgripadd, axis=1)

        print('Real elapsed time = %.2f' %total_time[-1])

        U = total_disp[0:2*(self.N+1),:]    # Extract displacement per timestep from ode solution 
        pos = U + self.inipos[:,None]       # Position per timestep

        # Empty arrays for forces and indices at each timestep                    
        columsnr = total_time.size                     # Total nr of timesteps
        sFcontact = np.zeros((2*(self.N+1),columsnr))  # Contact forces [Fcx, Fcy] (Friction, Normal)
        sFspring = np.zeros((2*(self.N+1),columsnr))   # Spring forces [Fsx, Fsy]  (internal forces) #TODO: change spring names to inner/outer? Confusing with internal/external forces
        sFspringext = np.zeros((2*(self.N+1),columsnr))# External spring forces Fs = Fsext + Fsint
        sFspringint = np.zeros((2*(self.N+1),columsnr))# Internal spring forces Fs = Fsext + Fsint
        sIndex = np.zeros((self.N+1,columsnr))         # Indices nodes in contact

        for ii in range(columsnr): #Calculate forces/indices using output ODE SOLVER
            sFspring[:,ii], sFspringext[:,ii], sFspringint[:,ii] = self.calculateFspring(U[:,ii]) #Fspring, Fspringext, Fspringint
            sFcontact[self.N+1:,ii], ind, sIndex[:,ii] = self.calculateFcontacty(U[:,ii]) # Contact force in y

        sFcontact[0:self.N+1] = total_disp[2*(self.N+1):,:] # Extract contact force in x from ode solution

        return U, fgrip, pos,columsnr, sFspring, sFcontact, total_time , total_sm, self.N

    def controlFunc(self, Fgrip, u0):
        stable_var = True       #Variable to quit ode when solver succeeded without SM < 0.2
        teller = 0              #Counts number of times solver ran
        safety_margins = []     #Empty list safety margins
        displacements = []      #Empty list displacements
        times = []              #Empty list elapsed time
        while stable_var:

            tsolve = Timer("solver %i" %(teller))
            tsolve.start()
            outputu = solve_ivp(self.dydx,(0,7),u0, args=[Fgrip], events=[self.sm_event_low_quit, self.sm_event_low_cont]) #Run solver, time interval is arbitrary
            tsolve.stop() 

            if not outputu.success: #Throw error if solver didn't complete
                print("Solver %i didn't solve completely. Last time step = %.2f sec" %(teller, outputu.t[-1]))
                print("MESSAGE %s" %(outputu.message))
            else: 
                print("Real solver time = %.2f" %outputu.t[-1]) #print real elapsed time to compare with computational time

            if outputu.message == 'A termination event occurred.': # Event occurs when SM < 02
                if outputu.t_events[0].size:
                    print('Safety margin < 0.2 in solver %i at time %.2f' %(teller, outputu.t[-1]))
                if outputu.t_events[1].size:
                    print('Safety margin > 0.2 in solver %i at time %.2f' %(teller, outputu.t[-1]))
            else:
                stable_var=False # Quit ode solver if ode completed without SM < 0.2

            U = outputu.y[0:2*(self.N+1),:]    # Displacement per timestep  
            SM = (self.fclb*Fgrip[self.N+1] - np.sum(outputu.y[2*(self.N+1)+1:-1,:], axis=0))/(self.fclb*Fgrip[self.N+1]) # Safety Margin

            safety_margins.append(SM)           #List of all ODE SOLVER outputs 
            displacements.append(outputu.y)     #List of all ODE SOLVER outputs
            times.append(outputu.t)             #List of all ODE SOLVER outputs

            self.P = np.append(self.P, Fgrip[self.N+1] - 0.01) # Apply more grip force #TODO: implement real controller. This only applies more grip when SM < 0.2.
            teller += 1

            # Set next ode inputs
            Fgrip[self.N+1] = self.P[teller]
            u0 = outputu.y[:,-1]

        return safety_margins, displacements, times 

    def dydx(self, t, U , Fgrip): 
        """ Main differential equations
            dx = c^-1*k*x - c^-1*Fextx
            dy = c^-1*k*y - c^-1*Fexty
            dFcx = Dahl friction  
            """
        Uxy = U[0:2*(self.N+1)]             # Displacement [x, y]
        Fxc = U[2*(self.N+1):3*(self.N+1)]  # Friction force

        Fspring, Fspringext, Fspringint = self.calculateFspring(Uxy)    # Spring forces
        Fyc, ind, indexvector = self.calculateFcontacty(Uxy)            # Contact forces (Returns force y direction and indices nodes in contact)
        Fext = np.concatenate((np.zeros(self.N+1),Fyc)) + np.concatenate((Fxc, np.zeros(self.N+1))) + Fgrip # Total external forces (Fcontact + Fgrip)
        
        dUxy = Fspring/self.c + Fext/self.c                 # Differential equation for dUx and dUy
        dUx = dUxy[0:self.N+1]                              # Displacement in x 
        dUFxc = self.calculateFcontactx(dUx, Fyc, Fxc, ind) # Calculate friction force in case of contact
            
        dU = np.concatenate((dUxy,dUFxc))                   # dU = [dx, dy, dFxc] (displacements & friciton force)
        dU[1] = dU[self.N] = dU[0]                          # Constrain rigid sensor part in x
        dU[self.N+2] = dU[2*(self.N+1)-1] = dU[self.N+1]    # Constrain rigid sensor part in y
        return dU

    def sm_event_low_quit(self,t, U, Fgrip): #TODO: Use events or add controller to state equation? See link on slack. 
        SM = (self.fclb*Fgrip[self.N+1] - np.sum(U[2*(self.N+1)+1:]))/(self.fclb*Fgrip[self.N+1]) # Calculate safety margin
        return SM - 0.2 #Functions, but SM fluctuates, so both positive and negative crossing occur around same timestep

    def sm_event_low_cont(self,t, U, Fgrip): #TODO: Use events or add controller to state equation? See link on slack. 
        SM = (self.fclb*Fgrip[self.N+1] - np.sum(U[2*(self.N+1)+1:]))/(self.fclb*Fgrip[self.N+1]) # Calculate safety margin
        return SM - 0.2 #Functions, but SM fluctuates, so both positive and negative crossing occur around same timestep

    def calculateFcontactx(self, dUx, Fyc, Fxc, ind):
        dUFxc = np.zeros(self.N+1)  # Empty array for friction force

        if ind[0].size != 0: # Dahl friction differential equation 
            dF1 = self.sigma0*dUx[ind[0]] 
            dF2 = np.power(np.abs(1+np.divide(Fxc[ind[0]],self.fclb*Fyc[ind[0]])*np.sign(dUx[ind[0]])),self.n) 
            dFsigns = np.sign(1+np.divide(Fxc[ind[0]],self.fclb*Fyc[ind[0]])*np.sign(dUx[ind[0]]))
            dUFxc[ind[0]] = - dF1*dF2*dFsigns  
        return dUFxc      

    def calculateFspring(self,U):
        Uxy = U[0:2*(self.N+1)] + self.inipos  # extract only x and y displacement from state vector TODO: easier to input just positions since I already split them in dxdy? Idem contact
        pos_x = Uxy[0:self.N+1]                # current x pos nodes
        pos_y = Uxy[self.N+1:]                 # current y pos nodes

        #EXTERNAL LAYER OF SPRINGS =======================================================================
        dext = np.sqrt(np.power(np.diff(pos_x[1:]), 2) + np.power(np.diff(pos_y[1:]), 2)) # Current segment length external springs
        kext_i = -(1-self.l0_ext/dext)*self.kext    # Change of length (N/m)
        Fspringext = np.zeros((2*(self.N+1)))      # Empty array for external forces Fspringext = [Fextx, Fexty]
        
        Fspringext[0] = - kext_i[0]*(pos_x[2]-pos_x[1]) - kext_i[self.N-2]*(pos_x[self.N-1]-pos_x[self.N])           #x0-x1/xn connection to rigid part
        Fspringext[self.N+1] = - kext_i[0]*(pos_y[2]-pos_y[1]) - kext_i[self.N-2]*(pos_y[self.N-1]-pos_y[self.N])   #y0-y1/yn connection to rigid part
        Fspringext[2:self.N] = - np.multiply(kext_i[0:-1], -np.diff(pos_x[1:self.N])) - np.multiply(kext_i[1:], np.diff(pos_x[2:]))   #General x (repeating nodes)
        Fspringext[self.N+3:-1] = - np.multiply(kext_i[0:-1], -np.diff(pos_y[1:self.N])) - np.multiply(kext_i[1:], np.diff(pos_y[2:]))#General y (repeating nodes)

        # Split forces for plotting (external springs in node 1 & N instead of 0) #TODO: remove? Not necessary to plot spring forces? 
        PlotFspringext = np.zeros((2*(self.N+1)))
        PlotFspringext[1] = - kext_i[0]*(pos_x[2]-pos_x[1])                             #node 1 x
        PlotFspringext[self.N] = - kext_i[self.N-2]*(pos_x[self.N-1]-pos_x[self.N]) #node N x
        PlotFspringext[self.N+2] = - kext_i[0]*(pos_y[2]-pos_y[1])                     #node 1 y
        PlotFspringext[-1] = - kext_i[self.N-2]*(pos_y[self.N-1]-pos_y[self.N])      #node N y
        PlotFspringext[2:self.N] = Fspringext[2:self.N]                               #x general, same as real forces
        PlotFspringext[self.N+3:-1] = Fspringext[self.N+3:-1]                         #y general, same as real forces

        #INTERNAL LAYER OF SPRINGS =======================================================================
        dint = np.sqrt(np.power(pos_x[0]-pos_x[2:self.N], 2) + np.power(pos_y[0]-pos_y[2:self.N], 2)) #Current length internal springs (exclude node 1 & N, bc they are part of rigid part)
        kint_i = -(1-self.l0_int/dint)*self.kint    # Change of length (N/m)
        Fspringint = np.zeros((2*(self.N+1)))      # Empty array for internal forces Fspringint = [Fintx, Finty]

        Fspringint[0] = - np.dot(kint_i, pos_x[2:self.N] - pos_x[0])           #x0 connection to rigid part
        Fspringint[self.N+1] = - np.dot(kint_i, pos_y[2:self.N] - pos_y[0])   #y0 connection to rigid part
        Fspringint[2:self.N] = - kint_i*(pos_x[0]-pos_x[2:self.N])    # General x (repeating nodes) 
        Fspringint[self.N+3:-1] = - kint_i*(pos_y[0]-pos_y[2:self.N]) # General y (repeating nodes) 
        
        Fspring = Fspringext + Fspringint #Add spring forces
        return Fspring, PlotFspringext, Fspringint

    def calculateFcontacty(self,U):
        pos = U + self.inipos                  # Extract position from state vector, TODO: easier to input just positions since I already split them in dxdy? Idem springs
        ind = np.where(pos[self.N+1:] <= 0)    # Contact if pos_y < 0 TODO: change this for different shapes object
        Fcy = np.zeros((self.N+1))             # Create empty force vector to fill just nodes in contact

        if ind[0].size != 0:
            Fcy[ind[0]] = - self.kc * (pos[ind[0]+self.N+1]) #Normal force (in case of contact)
        
        indexvector = np.zeros(self.N+1)    # Empty array indices nodes in contact
        indexvector[ind[0]] = 1             # Replace 0 with 1 if contact=True
        return Fcy, ind, indexvector
     
def main():
    sensor = SensorContact()
    SensorContact.sm_event_low_quit.terminal = True # Quit ode when SM becomes < 0.2
    SensorContact.sm_event_low_quit.direction = -1 #negative crossing

    SensorContact.sm_event_low_cont.terminal = False # Register SM becomes > 0.2, continue integration
    SensorContact.sm_event_low_cont.direction = 1 #positive crossing

    U, Fgrip, pos,t, sFspring, sFcontact, ttotal, SM, nr_nodes = sensor.solveODE()

    #Save data
    date_string = datetime.datetime.now().strftime("%d%m%H%M")
    workdir = os.getcwd()      #obtain current directory to save data
    f = (workdir + '\\SavedData\\data%s.npy' % date_string)
    save_dict = {'Displacements':U, 
                'Grip Force':Fgrip, 
                'Positions':pos,
                'Nr of timesteps':t,
                'Spring Force':sFspring,
                'Contact Force':sFcontact,
                'Time vector':ttotal,
                'Safety Margin':SM,
                'nr_nodes':nr_nodes}
    np.save(f, save_dict)

    # Visualise deformation
    savevar = False
    visual = AnimationClass(U,Fgrip,pos,t,sFspring,sFcontact,ttotal, SM, nr_nodes, savevar)
    visual.run()

if __name__ == "__main__":
    main()